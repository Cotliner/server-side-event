package cotliner.serversideevent.document.event.enums

enum class EventType {
  ORDER_CREATED,
  ORDER_UPDATED,
}