package cotliner.serversideevent.document.event

import cotliner.serversideevent.document.event.enums.EventType
import java.util.UUID

sealed class Event {
  abstract val type: EventType
  data class StandardEvent<T>(val content: T, override val type: EventType, val buyerMail: String? = null): Event()
}
