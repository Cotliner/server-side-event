package cotliner.serversideevent.document.order.dto

data class OrderInputDto(val buyerMail: String?, val status: String?)