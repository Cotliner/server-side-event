package cotliner.serversideevent.service

import cotliner.serversideevent.common.SearchParam
import cotliner.serversideevent.document.event.Event
import cotliner.serversideevent.document.event.Event.StandardEvent
import cotliner.serversideevent.document.event.enums.EventType.ORDER_CREATED
import cotliner.serversideevent.document.event.enums.EventType.ORDER_UPDATED
import cotliner.serversideevent.document.order.Order
import cotliner.serversideevent.document.order.dto.OrderInputDto
import cotliner.serversideevent.repository.OrderRepository
import kotlinx.coroutines.*
import kotlinx.coroutines.CoroutineStart.LAZY
import kotlinx.coroutines.Dispatchers.Default
import kotlinx.coroutines.Dispatchers.IO
import kotlin.random.Random.Default.nextDouble
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.*
import org.springframework.data.domain.PageRequest
import org.springframework.stereotype.Service
import java.util.*
import java.util.UUID.randomUUID
import kotlin.random.Random.Default.nextInt

@Service class OrderService(
  /* SHARED FLOW */
  private val eventFlow: MutableSharedFlow<Event>,
  private val orderMailer: Channel<Order>,
  private val simpleMailer: Channel<String>,
  /* REPOSITORIES */
  private val orderRepository: OrderRepository
) {
  fun findAll(): Flow<Order> = orderRepository.findAll()

  suspend fun create(orderToCreate: OrderInputDto): Order = orderRepository.save(Order(
    randomUUID(),
    "CREATED",
    nextDouble(1.0, 500.0),
    orderToCreate.buyerMail!!
  )).also {
    eventFlow.emit(StandardEvent(it.id, ORDER_CREATED, it.buyerMail))
    orderMailer.send(it)
    CoroutineScope(Default).launch { randomMail().collect() }
  }

  suspend fun update(
    orderId: UUID,
    orderToUpdate: OrderInputDto
  ): Order = with(
    orderRepository.findById(orderId) ?: throw Exception()
  ) {
    this.status = orderToUpdate.status!!
    orderRepository.save(this)
  }.also {
    eventFlow.emit(StandardEvent(it.id, ORDER_UPDATED, it.buyerMail))
    orderMailer.send(it)
    CoroutineScope(Default).launch { randomMail().collect() }
  }

  fun search(
    param: SearchParam,
    request: PageRequest
  ): Flow<Order> = with(
    param
  ) { orderRepository.findAllByPriceBetween(startPrice, endPrice, request) }

  suspend fun searchCount(
    param: SearchParam
  ): Number = with(
    param
  ) { orderRepository.countByPriceBetween(startPrice, endPrice) }

  suspend fun delete(id: UUID): Unit = orderRepository.deleteById(id)

  private fun randomMail(): Flow<Int> = (1..999).asFlow().onEach { simpleMailer.send("${randomUUID()}@europcar.com") }
}