package cotliner.serversideevent.service

import cotliner.serversideevent.document.event.Event
import cotliner.serversideevent.document.event.Event.StandardEvent
import cotliner.serversideevent.document.event.enums.EventType.ORDER_UPDATED
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.buffer
import kotlinx.coroutines.flow.filter
import org.springframework.stereotype.Service
import java.util.*

@Service class EventService(
  /* SHARED FLOW */
  private val eventFlow: MutableSharedFlow<Event>,
) {
  fun getAll(buyerMail: String): Flow<Event> = eventFlow.filter { it.rules(buyerMail) }

  private fun Event.rules(buyerMail: String): Boolean = when (this) {
    is StandardEvent<*> -> simpleEventRules(buyerMail)
  }

  private fun StandardEvent<*>.simpleEventRules(buyerMail: String): Boolean = buyerMail == this.buyerMail
}