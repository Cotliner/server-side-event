package cotliner.serversideevent;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ServerSideEventApplication {
	public static void main(String[] args) {
		SpringApplication.run(ServerSideEventApplication.class, args);
	}
}
