package cotliner.serversideevent.config

import cotliner.serversideevent.document.event.Event
import cotliner.serversideevent.document.order.Order
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.consumeEach
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.launch
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.mail.SimpleMailMessage
import org.springframework.mail.javamail.JavaMailSender

@Configuration open class SharedDataConfig(
  /* PROPERTIES */
  @Value("\${spring.mail.values.notification}") private val notification: String,
  /* BEANS */
  private val mailSender: JavaMailSender
) {
  @Bean open fun eventFlow(): MutableSharedFlow<Event> = MutableSharedFlow()

  @DelicateCoroutinesApi
  @Bean open fun orderMailer(): Channel<Order> = Channel<Order>().consumeWith { it.sendMail() }

  @DelicateCoroutinesApi
  @Bean open fun simpleMailer(): Channel<String> = Channel<String>().consumeWith { it.sendMail() }

  private fun Order.sendMail(): Unit = with(SimpleMailMessage()) {
    from = notification
    setTo(buyerMail)
    subject = "Order with id $id"
    text = "Status: $status"
    mailSender.send(this)
  }

  private fun String.sendMail(): Unit = with(SimpleMailMessage()) {
    from = notification
    setTo(this@sendMail)
    subject = "Order updated"
    text = "Status updated"
    mailSender.send(this)
  }

  @DelicateCoroutinesApi
  private fun <T> Channel<T>.consumeWith(
    consumer: (it: T) -> Unit
  ): Channel<T> = this.also { GlobalScope.launch { consumeEach(consumer::invoke) } }
}