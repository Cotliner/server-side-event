package cotliner.serversideevent.common

data class SearchParam(
  val startPrice: Number = 0,
  val endPrice: Number = Int.MAX_VALUE
)
